
/**
 *Feb 8, 2018
 *org.maps.wgis.cookiehandler
 *hims-webservices
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.maps.wgis.cookiehandler;

import java.io.Serializable;
import java.util.List;

public class CookieModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String key;
	public List<String> value;

	/**
	 * @param key
	 * @param value
	 */
	public CookieModel(String key, List<String> value) {
		super();
		this.key = key;
		this.value = value;
	}

}
